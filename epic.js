"use strict";

(function() {

    let imageData = [];
    let currentImageType = "natural";

    document.addEventListener("DOMContentLoaded", function(event) {
        
        recentAvailableDates();
        const imageMenu = document.getElementById("image-menu");

        /**
         * Creates a search URL based on the type of image and date.
         * @param {string} type - The type of the image.
         * @param {string} date - The date of the image.
         * @returns {string} The URL to fetch image data.
         */
        function createSearchURL(type, date) {
            let baseURL = 'https://epic.gsfc.nasa.gov/api';
            return `${baseURL}/${type}/date/${date}`;
        }

        /**
         * Updates the image menu with available image data.
         */
        function updateImageMenu() {
            const imageMenu = document.getElementById("image-menu");
            imageMenu.innerHTML = "";

            if (imageData.length === 0) {
                let li = document.createElement("li");
                li.textContent = "No images available";
                imageMenu.appendChild(li);
            } else {
                imageData.forEach((image, index) => {
                    let li = document.createElement("li");
                    li.textContent = image.date;
                    li.setAttribute("data-index", index);
                    imageMenu.appendChild(li);
                });
            }
        }

        /**
         * Displays the selected image based on the user's click.
         * @param {Event} event - The event object.
         */
        function displayImage(event) {
            const listLi = Array.from(event.currentTarget.children);
            let liIndex = 0; 

            for(let i = 0; i < listLi.length; i++){
                if(listLi[i].textContent.includes(event.target.textContent)){
                    liIndex = i;
                    break; 
                }
            }
           
            const selectedData = imageData[liIndex];
            const yearMonthDay = selectedData.date.substring(0, 10).split("-");
            const year = yearMonthDay[0]; 
            const month = yearMonthDay[1];
            const day = yearMonthDay[2];
            
            const type = document.getElementById("type").value;
            const imageName = `${selectedData.image}.jpg`;
            const imageUrl = `https://epic.gsfc.nasa.gov/archive/${type}/${year}/${month}/${day}/jpg/${imageName}`;
            
            document.getElementById("earth-image").src = imageUrl;
        }
        imageMenu.addEventListener("click", displayImage);

        document.getElementById("submitButton").addEventListener("click", function(event) {
            event.preventDefault();

            currentImageType = document.getElementById("type").value;
            let date = document.getElementById("date").value;
            let URL = createSearchURL(currentImageType, date);

            fetch(URL)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(`Something went wrong`);
                    }
                    return response.json();
                })
                .then(obj => {
                    imageData = obj;
                    updateImageMenu();
                    console.log(obj);
                })
                .catch(err => {
                    console.error("Error:", err);
                });
        });

        /**
         * Fetches and updates the input field with the most recent available dates for images.
         */
        function recentAvailableDates() {
            let datesURL = `https://epic.gsfc.nasa.gov/api/${currentImageType}/all`;

            fetch(datesURL)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(`HTTP error! status: ${response.status}`);
                    }
                    return response.json();
                })
                .then(obj => {
                    let mostRecentDate = obj[0].date;
                    document.getElementById("date").max = mostRecentDate;
                })
                .catch(err => {
                    console.error("Error fetching dates:", err);
                });
        }

        document.getElementById("type").addEventListener("change", function(event) {
            currentImageType = event.target.value;
            recentAvailableDates();
        });
    });
})();